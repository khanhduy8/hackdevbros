---
headless: true
---

- [**Red Team Skills**]({{< relref "/docs/redteam_skills" >}})
- [Privilege Escalation]({{< relref "/docs/redteam_skills/privesc" >}})
  - [Cron Jobs]({{< relref "/docs/redteam_skills/privesc/cronjob" >}})